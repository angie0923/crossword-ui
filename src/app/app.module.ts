import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CrosswordGridComponent } from './components/grid/grid.component';
// import { MatGridListModule} from '@angular/material';
import { CluesComponent } from './components/clues/clues.component';


@NgModule({
  declarations: [
    AppComponent,
    CrosswordGridComponent,
    CluesComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // MatGridListModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
