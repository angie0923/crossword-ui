import { Injectable } from '@angular/core';
import { Tile } from "../models/Tile";
import * as _ from 'lodash';

@Injectable({
providedIn: 'root'
})
export class TileService {

responses: any;

constructor() { }

getTiles(): Tile[] {
    let tiles: Tile[] = [];
    // let tiles: Tile[] = [
    //   {color: 'while', editable: true, rows: 0, cols: 0},
    //   {color: 'black', editable: false, rows: 0, cols: 0},
    //   {color: 'white', editable: true, rows: 0, cols: 0},
    //   {color: 'black', editable: false, rows: 0, cols: 0},
    // ];
    let grid = this.getGrid();
    let tileIndex = 0;
    for (let i=0; i<grid.length; i++) {
    let row = grid[i];
    for (let j=0; j<row.length; j++) {
        let value = row[j];
        let isEditable = value?true:false;
        let color = value?'white':'black';
        // let placeholder = this.getPlaceholder(i, j);
        tiles.push({
        color: color,
        editable: isEditable,
        rows: i,
        cols: j,
        value: row[j],
        // placeholder: placeholder
        });
    }
    }
    // this.responses = tiles;
    this.responses = tiles.map(x => Object.assign({}, x));
    return tiles;
}

getResponses() {
    return this.responses;

}

updateResponses(responseTile: Tile) {
    console.log(responseTile.rows +',' + responseTile.cols)
    let idx = _.findIndex(this.responses, {'rows': responseTile.rows, 'cols': responseTile.cols})
    this.responses[idx] = responseTile;
    console.log(idx);
    console.log(this.responses);
}



getGrid(): any {
let tiles = [
    ['w','a','i','l','i','n','g'],
    ['h',null,'m',null,'c',null,'o'],
    ['i','n','b','r','e','e','d'],
    ['t',null,'i',null,'f',null,'h'],
    ['e','m','b','a','r','g','o'],
    ['n',null,'e',null,'e',null,'o'],
    ['s','u','s','p','e','n','d']
    ];
    return tiles;
}
}
